var http = require('http');
var url = require('url');
var fs = require('fs');
// var url_parts = url.parse(req.url, true);

http.createServer(function (request, response) {

    if (request.url === '/') {
        fs.readFile('server/index.html', function (err, data) {
            response.end(data);
        });
    } else {
        fs.readFile('server' + request.url, function (err, data) {
            if (!err) {
                response.end(data);
            } else {
                response.writeHead(404, {'Content-Type': 'text/plain'});
                response.write('Resource not found');
                response.end();
            }
        });
    }

}).listen(3000);


