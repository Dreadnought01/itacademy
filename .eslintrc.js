module.exports = {
    "env": {
        "browser": true
    },
    "globals": {
        "$": true,
        "_": true,
        "mediator": true,
        "templates": true,
        "template": true,
		"controller": true
    },
    "extends": "eslint:recommended",
    "rules": {
        "indent": [
            "error",
            4
        ],
        "quotes": [
            "error",
            "single"
        ],
        "semi": [
            "error",
            "always"
        ]
    }
};