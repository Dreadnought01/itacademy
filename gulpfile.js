var gulp = require('gulp');
var $ = require('gulp-load-plugins')();
var lazypipe = require('lazypipe');
var runSequence = require('run-sequence');

gulp.task('build', function () {
    runSequence(['clean:build', 'test'],
        'minimize',
        'resources:build')
});

gulp.task('debug', function () {
    runSequence(['clean:debug', 'test'],
        'minimize-for-debug',
        'resources:debug')
});

gulp.task('minimize', function () {
    return gulp.src('src/index.html')
        .pipe($.useref())
        .pipe($.if('app.js', $.eslint(), $.eslint.format()))
        .pipe($.if('*.js', $.uglify()))
        .pipe($.if('*.css', $.htmlmin({collapseWhitespace: true, removeComments: true})))
        .pipe(gulp.dest('client'));
});

gulp.task('minimize-for-debug', function () {
    return gulp.src('src/index.html')
        .pipe($.useref({}, lazypipe().pipe($.sourcemaps.init, {loadMaps: true})))
        // .pipe($.if('*.js', $.uglify()))
        .pipe($.if('*.css', $.htmlmin({collapseWhitespace: true, removeComments: true})))
        .pipe($.sourcemaps.write())
        .pipe(gulp.dest('server'));
});

gulp.task('lint', function () {
    return gulp.src('src/**/*.js')
        .pipe($.eslint())
        .pipe($.eslint.format());
});

gulp.task('test', function () {
    return gulp.src('test/tests.html')
        .pipe($.qunit());
});

gulp.task('resources:build', function () {
    return gulp.src('src/resources/**/*')
        .pipe(gulp.dest('client'));
});

gulp.task('resources:debug', function () {
    return gulp.src('src/resources/**/*')
        .pipe(gulp.dest('server'));
});

gulp.task('clean:build', function () {
    return gulp.src('client')
        .pipe($.clean())
});

gulp.task('clean:debug', function () {
    return gulp.src('server')
        .pipe($.clean())
});