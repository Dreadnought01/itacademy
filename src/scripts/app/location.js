'use strict';

var Location = (function () {
    var _id = 0;

    function Constructor (locality, groupNum, teacherNum) {
        var _location = {
            id: _id++,
            locality: locality,
            groupNum: groupNum,
            teacherNum: teacherNum
        };

        this.get = function () {
            return {
                id: _location.id,
                locality: _location.locality,
                groupNum: _location.groupNum,
                teacherNum: _location.teacherNum
            };
        };

        this.set = function (location) {
            _location = location;
        };
    }

    return Constructor;
})();
