'use strict';

var ModalView = (function () {
    var _$el;

    function Constructor ($el) {
        _$el = $el;

        return this;
    }

    Constructor.prototype.show = function () {
        _$el.html(template(templates['modal']));
    };

    Constructor.prototype.hide = function () {
        _$el.empty();
    };
    
    return Constructor;
})();
