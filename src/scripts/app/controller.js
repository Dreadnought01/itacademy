'use strict';

function Controller () {
    init();

    function init () {
        var locationsView,
            $locationsEl,
            formSelector,
            locations,
            modalView,
            formView,
            $modalEl;

        formSelector = '.modal-content';
        $locationsEl = $('#locations');
        $modalEl = $('#modal');

        locations = new Locations();
        modalView = new ModalView($modalEl);
        locationsView = new LocationsView(locations, $locationsEl);
        formView = new FormView(formSelector);

        locationsView.render();

        mediator.subscribe('locationsModel update', function () {
            locationsView.render();
        });

        mediator.subscribe('locationsModel add', renderOne);

        mediator.subscribe('start edit/create', modalView.show)
            .subscribe('start edit/create', formView.render)
            .subscribe('start edit/create', locationsView.disable);

        mediator.subscribe('accept create', locations.create);

        mediator.subscribe('accept edit', locations.edit);

        mediator.subscribe('click remove', locations.remove);

        mediator.subscribe('close form', formView.clear)
            .subscribe('close form', modalView.hide)
            .subscribe('close form', locationsView.enable);

        function renderOne (location) {
            var locationView = new LocationView($locationsEl.find('.locations'));
            locationView.render(location.get());
        }

    }

    return this;
}