'use strict';

var FormView = (function () {
    var _selector,
        _$el;

    function Constructor (selector) {
        _selector = selector;

        return this;
    }

    Constructor.prototype.render = function (location) {
        _$el = $(_selector);

        _$el.html(template(templates['form'], location));

        _$el.find('.save').on('click', save);
        _$el.find('.cancel').on('click', function () {
            mediator.publish('close form');
        });

        $(document).on('keydown', keyEvent);

        function save () {
            var $teacherNumEl,
                $localityEl,
                $groupNumEl;

            $teacherNumEl = _$el.find('.teacherNum');
            $localityEl = _$el.find('.locality');
            $groupNumEl = _$el.find('.groupNum');

            if (location) {
                mediator.publish('accept edit', {
                    id: location.id,
                    locality: $localityEl.val(),
                    groupNum: $groupNumEl.val(),
                    teacherNum: $teacherNumEl.val()
                });
            } else {
                mediator.publish('accept create', $localityEl.val(), $groupNumEl.val(), $teacherNumEl.val());
            }

            mediator.publish('close form');

            return false;
        }

        function keyEvent (event) {
            var enterCode = 13,
                escCode = 27;

            if (event.which === enterCode) {
                save();
            } else if (event.which === escCode) {
                mediator.publish('close form');
            }
        }
    };

    Constructor.prototype.clear = function () {
        _$el.find('.save').off('click')
            .end().find('.cancel').off('click')
            .end().empty();
        $(document).off('keydown');
    };

    return Constructor;
})();
