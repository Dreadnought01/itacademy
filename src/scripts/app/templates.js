'use strict';

var templates = {
    locations: [
        '<ul class="locations row"></ul>',
        '<button class="add btn btn-success">Add</button>'
    ].join(''),

    location: [
        '<section class="location">',
        '<button class="btn btn-warning edit">Edit</button>',
        '<button class="btn btn-danger remove">Remove</button>',
        '<div><%locality%></div>',
        '<div><%groupNum%> / <%teacherNum%></div>',
        '</section>'
    ].join(''),

    form: [
        '<div>Locality: <input type="text" class="form-control locality" value="<%locality%>"></div>',
        '<div>Group`s number: <input type="text" class="form-control groupNum" value="<%groupNum%>"></div>',
        '<div>Teacher`s number: <input type="text" class="form-control teacherNum" value="<%teacherNum%>"></div>',
        '<button class="save btn btn-success">Save</button>',
        '<button class="cancel btn btn-warning">Cancel</button>'
    ].join(''),

    modal: [
        '<section class="modal fade in">',
        '<div class="modal-dialog modal-sm">',
        '<div class="modal-content"></div>',
        '</div>',
        '</section>'
    ].join('')
};