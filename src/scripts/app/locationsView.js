'use strict';

var LocationsView = (function () {
    var _locations,
        _$el;

    function Constructor (locations, $el) {
        _locations = locations;
        _$el = $el;

        return this;
    }

    Constructor.prototype.render = function () {
        var $ul,
            $btn;

        (_$el).empty();

        _$el.html(template(templates['locations']));

        $btn = _$el.find('.add');

        $btn.on('click', function () {
            mediator.publish('start edit/create');
        });
        
        $ul = _$el.find('.locations');

        _locations.iterate(function (location) {
            var locationView = new LocationView($ul);
            locationView.render(location.get());
        });
    };

    Constructor.prototype.disable = function () {
        _$el.addClass('disabled');
    };

    Constructor.prototype.enable = function () {
        _$el.removeClass('disabled');
    };

    return Constructor;
})();
