'use strict';

var LocationView = (function () {
    function Constructor ($el) {
        this.$el = $el;

        return this;
    }

    Constructor.prototype.render = function (location) {
        var $li;

        $li = $('<li>', {class: 'col-sm-4'});

        $li.html(template(templates['location'], location));

        $li.find('.remove').on('click', function () {
            mediator.publish('click remove', location.id);
            
            return false;
        });

        $li.find('.edit').on('click', function () {
            mediator.publish('start edit/create', location);
            
            return false;
        });

        this.$el.append($li);
    };

    return Constructor;
})();