'use strict';

var Locations = (function () {
    var _locations = {
        0: new Location('Dnipropetrovsk', 5, 3),
        1: new Location('Kyiv', 2, 1),
        2: new Location('Lviv', 12, 9),
        3: new Location('Ivano-Frankivsk', 3, 2),
        4: new Location('Chernivtsi', 2, 2),
        5: new Location('Rivne', 1, 1)
    };

    function Constructor () {

        return this;
    }

    Constructor.prototype.remove = function (id) {
        delete _locations[id];

        mediator.publish('locationsModel update');
    };

    Constructor.prototype.create = function (locality, groupNum, teacherNum) {
        var location = new Location(locality, groupNum, teacherNum);

        _locations[location.get().id] = location;

        mediator.publish('locationsModel add', location);
    };

    Constructor.prototype.edit = function (location) {
        _locations[location.id].set(location);

        mediator.publish('locationsModel update');
    };

    Constructor.prototype.iterate = function (fn) {
        _.each(_locations, function (location) {
            fn(location);
        });
    };

    return Constructor;
})();

