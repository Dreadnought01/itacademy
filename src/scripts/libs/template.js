function template (template, data) {
    data = data || {};

    Object.keys(data).forEach(function (key) {
        template = template.replace('<%' + key + '%>', data[key]);
    });

    template = template.replace(/<%([\s\S]+?)%>/g, '');

    return template;
}