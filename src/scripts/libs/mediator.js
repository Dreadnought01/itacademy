'use strict';

function Mediator () {
    this.channels = {};

    this.subscribe = function (channel, fn) {
        if (!this.channels[channel]) this.channels[channel] = [];
        this.channels[channel].push(fn);

        return this;
    };

    this.publish = function (channel) {
        var args;

        args = Array.prototype.slice.call(arguments, 1);

        this.channels[channel].forEach(function (item) {
            item.apply(this, args);
        });
    };

    return this;
}