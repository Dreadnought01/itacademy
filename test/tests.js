QUnit.config.reoder = false; //order of tests run is important

QUnit.module('Task - Mediator)', function () {
    var mediator;

    mediator = new Mediator();

    QUnit.test("test 1 – creating and publishing channel", function (assert) {
        var flag = false;

        mediator.subscribe('test1', function () {
            flag = true;
        });

        mediator.publish('test1');

        assert.equal(flag, true);
    });

    QUnit.test("test 2 – channel with 1 function and 2 arguments", function (assert) {
        var sum;

        mediator.subscribe('test2', function (firstNum, secondNum) {
            sum = firstNum + secondNum;
        });

        mediator.publish('test2', 3, 5);

        assert.equal(sum, 8);
    });

    QUnit.test("test 3 – channel with some functions and arguments", function (assert) {
        var changedPhrase,
            fullPhrase;

        mediator.subscribe('test3', function (firstSentence, secondSentence) {
            fullPhrase = firstSentence.concat(secondSentence);
        });

        mediator.subscribe('test3', function (firstSentence) {
            changedPhrase = firstSentence.slice(0, 5);
        });

        mediator.publish('test3', 'QUnit was developed as part of jQuery.', 'Now QUnit runs standalone.');

        assert.deepEqual([fullPhrase, changedPhrase], ['QUnit was developed as part of jQuery.Now QUnit runs standalone.',
            'QUnit']);
    });

    QUnit.test("test 4 – some channels are created", function (assert) {
        var channelsQuantity = Object.keys(mediator.channels).length;

        assert.equal(channelsQuantity, 3, 'There are 3 channels created');
    });
});


